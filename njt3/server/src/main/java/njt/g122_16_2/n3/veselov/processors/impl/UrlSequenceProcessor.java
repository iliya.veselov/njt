package njt.g122_16_2.n3.veselov.processors.impl;

import njt.g122_16_2.n3.veselov.processors.WordSequenceProcessor;
import org.apache.commons.validator.routines.UrlValidator;

import java.util.ArrayList;
import java.util.IllegalFormatException;
import java.util.List;
import java.util.stream.Collectors;

import static org.apache.commons.validator.routines.UrlValidator.ALLOW_ALL_SCHEMES;

public class UrlSequenceProcessor implements WordSequenceProcessor {
    private final UrlValidator validator = new UrlValidator(ALLOW_ALL_SCHEMES);

    @Override
    public String process(String inputSequence) throws IllegalFormatException {
        List<Boolean> outputSequence = new ArrayList<>();
        for (String url : inputSequence.split("[\\n \\t]")) {
            outputSequence.add(validator.isValid(url));
        }
        return outputSequence.stream().map(Object::toString).collect(Collectors.joining(" "));
    }
}
