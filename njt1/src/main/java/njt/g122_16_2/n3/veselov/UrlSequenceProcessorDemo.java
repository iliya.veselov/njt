package njt.g122_16_2.n3.veselov;

import njt.g122_16_2.n3.veselov.processors.WordSequenceProcessor;
import njt.g122_16_2.n3.veselov.processors.impl.UrlSequenceProcessor;

public class UrlSequenceProcessorDemo {
    public static void main(String[] args) {
        System.out.println("==== WordToLengthConvertorDemo");
        System.out.println("==== Convert sequence of words to their lenghts");
        WordSequenceProcessor processor = new UrlSequenceProcessor();

        boolean yes;
        boolean no;
        do {
            System.out.print("<<<< Enter sequence of words: ");
            String sequence = System.console().readLine();
            String result = processor.process(sequence);
            System.out.printf(">>>> Result sequence: %s%n", result);
            for (; ; ) {
                System.out.print("<<<< Continue? (Y/N): ");
                String response = System.console().readLine();
                yes = response.toUpperCase().equals("Y");
                no = response.toUpperCase().equals("N");
                if (yes || no) {
                    break;
                }
                System.out.println("!!!! Your choice should be just Y or N. Please try again");
            }
        } while (yes);
        System.out.print("==== Bye");
    }

}


