package njt.g122_16_2.n3.veselov;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class TcpClient {

    public static void main(String[] args) throws IOException {
        System.out.println("==== UrlSequenceProcessor - TCP Client Demo");
        System.out.println("==== Convert sequence of words to their lenghts");

        boolean yes, no;
        do {
            Socket socket = new Socket("localhost", 26203);

            DataOutputStream oos = new DataOutputStream(socket.getOutputStream());
            DataInputStream ois = new DataInputStream(socket.getInputStream());

            System.out.print("<<<< Enter sequence of words: ");
            String sequence = System.console().readLine();

            oos.writeUTF(sequence);
            oos.flush();

            String result = ois.readUTF();
            System.out.printf(">>>> Result sequence: %s%n", result);

            for (; ; ) {
                System.out.print("<<<< Continue? (Y/N): ");
                String response = System.console().readLine();
                yes = response.toUpperCase().equals("Y");
                no = response.toUpperCase().equals("N");
                if (yes || no) {
                    break;
                }
                System.out.println("!!!! Your choice should be just Y " +
                        "or N. Please try again");
            }
        } while (yes);
        System.out.print("==== Bye");
    }


}


