package njt.g122_16_2.n3.veselov;

import njt.g122_16_2.n3.veselov.processors.WordSequenceProcessor;
import njt.g122_16_2.n3.veselov.processors.impl.UrlSequenceProcessor;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

public class UdpServer {

    private static final int PORT = 26203;

    public static void main(String[] args) throws IOException {

        //Step #1 - Print Server info
        System.out.printf("UDP Server> Start on port %d%n", PORT);

        // Step #2 - Create Server socket
        DatagramSocket socket = new DatagramSocket(PORT);

        // Step #3 - Prepare a storage for the incoming messages
        byte[] buffer = new byte[65536];
        DatagramPacket incoming = new DatagramPacket(buffer, buffer.length);

        while (true) {
            // Step #4 - Receive data from the client
            System.out.printf("UDP Server> Waiting for a incoming data...");
            socket.receive(incoming);
            String clientData = new String(incoming.getData(), 0,
                    incoming.getLength());
            System.out.printf("UDP Server> Received client's data: %s%n",
                    clientData);

            // Step #5 - Process client's data and prepare result
            WordSequenceProcessor processor = new UrlSequenceProcessor();
            String result = processor.process(clientData);
            System.out.println("UDP Server> Prepared result: " + result);

            // Step #6 - Send result to the client
            DatagramPacket outcoming = new DatagramPacket(
                    result.getBytes(),
                    result.getBytes().length,
                    incoming.getAddress(),
                    incoming.getPort());

            socket.send(outcoming);
            System.out.println("UDP Server> Result has been sent");

            System.out.println("UDP Server> Session ended");
            // Step #7 - Repeat from the Step #4
        }
    }
}
