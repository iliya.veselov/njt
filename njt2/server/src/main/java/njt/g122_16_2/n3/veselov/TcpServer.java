package njt.g122_16_2.n3.veselov;

import njt.g122_16_2.n3.veselov.processors.WordSequenceProcessor;
import njt.g122_16_2.n3.veselov.processors.impl.UrlSequenceProcessor;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class TcpServer {

    private static final int PORT = 26203;

    public static void main(String[] args) throws IOException {

        //Step #1 - Print Server info
        System.out.printf("Server> Start on port %d%n", PORT);

        // Step #2 - Create Server socket
        ServerSocket server = new ServerSocket(PORT);

        while (true) {
            // Step #3 - Accept client's connection, create new socket
            System.out.printf("Server> Waiting for a new connection ... ");
            Socket client = server.accept();
            System.out.println("connection accepted");

            // Step #4 - Prepare input/output streams
            DataOutputStream out = new DataOutputStream(client.getOutputStream());
            DataInputStream in = new DataInputStream(client.getInputStream());

            // Step #5 - Receive data from the client
            String clientData = in.readUTF();
            System.out.println("Server> Received client's data: " + clientData);

            // Step #6 - Process client's data and prepare result
            WordSequenceProcessor processor = new UrlSequenceProcessor();
            String result = processor.process(clientData);
            System.out.println("Server> Prepared result: " + result);

            // Step #7 - Send result to the client
            out.writeUTF(result);
            out.flush();
            System.out.println("Server> Result has been sent");

            in.close();
            out.close();

            client.close();
            System.out.println("Server> Session ended");
            // Step #8 - Repeat from the Step #3
        }
    }
}


