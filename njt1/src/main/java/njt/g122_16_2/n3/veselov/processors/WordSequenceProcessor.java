package njt.g122_16_2.n3.veselov.processors;

import java.util.IllegalFormatException;

public interface WordSequenceProcessor {
    String process(String inputSequence) throws IllegalFormatException;
}
