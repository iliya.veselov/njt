package njt.g122_16_2.n3.veselov;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class UdpClient {

    private static final int PORT = 26203;

    public static void main(String[] args) throws IOException {
        System.out.println("==== UrlSequenceProcessor - UDP Client Demo");
        System.out.println("==== Convert sequence of words to their lengths");

        boolean yes, no;
        do {
            DatagramSocket socket = new DatagramSocket();

            System.out.print("<<<< Enter sequence of words: ");
            String sequence = System.console().readLine();
            byte[] outputData = sequence.getBytes();

            DatagramPacket outputDatagram = new DatagramPacket(
                    outputData,
                    outputData.length,
                    InetAddress.getByName("localhost"), PORT);
            socket.send(outputDatagram);

            byte[] inputData = new byte[65536];
            DatagramPacket inputDatagram = new DatagramPacket(
                    inputData, inputData.length);
            socket.receive(inputDatagram);
            String result = new String(inputDatagram.getData(), 0,
                    inputDatagram.getLength());
            System.out.printf(">>>> Result sequence: %s%n", result);

            for (; ; ) {
                System.out.print("<<<< Continue? (Y/N): ");
                String response = System.console().readLine();
                yes = response.toUpperCase().equals("Y");
                no = response.toUpperCase().equals("N");
                if (yes || no) {
                    break;
                }
                System.out.println("!!!! Your choice should be just Y " +
                        "or N. Please try again");
            }
        } while (yes);
        System.out.print("==== Bye");
    }

}



